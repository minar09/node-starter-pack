/**
 * Created by asci on 1/27/16.
 */
(function(){
    'use strict';
    var route=angular.module('routerApp',['ui.router']);
    route.config(function($stateProvider,$urlRouterProvider){
        $urlRouterProvider.otherwise('/login');
        $stateProvider
            .state('login',{
                url:'/login',
                templateUrl:'views/authentication/login.html'
            })
            .state('signup',{
                url:'/signup',
                templateUrl:'views/authentication/signup.html'
            });
    })
})();